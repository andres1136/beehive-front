const url = 'http://localhost/8000';

export const login = url + 'api/login';
export const registerUser = url + 'api/register';
export const stores = url + 'api/stores';
export const store = url + 'api/store';
export const registerAccess = url + 'api/register-access';
export const registerUsers = url + 'api/register-users';
export const lastRegisterUsers = url + 'api/last-user';